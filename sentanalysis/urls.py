from django.conf.urls import patterns, include, url, static
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sentanalysis.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^sentiment/', include('sentiment.urls')),
)
