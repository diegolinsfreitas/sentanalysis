__author__ = 'diego'

from collections import Counter
import tweepy
from anylizer import *
import time
from django.utils import timezone
from sentiment.models import Sentiment
from sentanalysis.settings import *


def build_api():
    auth = tweepy.OAuthHandler('C5lYZMKPHbUzYlyhZCwpcGdUm', '1aS4z79mQ426rJZe19hwppxmDSQfTMKmBAMqpAZOJ2goheST75')

    auth.set_access_token('2779478684-B3WKNIlKY5TeNimeACIIXT31GN1owoOr2dyZvWH',
                          'wLWCPOj9fLIzXnToFJigCasOT2f23oaFGqdggCcy5lLuc')

    return tweepy.API(auth)


class SentimentsLoader():
    def load_sentiments_from_twitter(self):
        api = build_api()

        last_tweet_id = ""
        total_sentiments = 0
        pos_sentiments = 0
        neg_sentiments = 0
        # while true:
        sent_classf = SentimentClassifier(BASE_DIR + "/classifier.pkl")
        for i in range(20):
            results = api.search(geocode='-3.060002,-59.991321,20km', count=100, since_id=last_tweet_id)
            if len(results) == 0:
                time.sleep(2)
                continue
            last_tweet_id = results[0].id_str
            sentiments_classification = sent_classf.classify_tweets(results)
            for classification in sentiments_classification:
                Sentiment(feeling=classification, pub_date=timezone.now()).save()
            time.sleep(2)