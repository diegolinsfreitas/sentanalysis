import csv
import re
import os

import nltk
from nltk.corpus import stopwords
from sklearn.externals import joblib


def getFeatures(doc):
    docWords = set(doc)
    feat = {}
    for word in wordFeatures:
        feat['contains(%s)' % word] = (word in docWords)
    return feat

class SentimentClassifier(object):
    model = None
    corpus_tweets = None

    stopWords = stopwords.words('portuguese')

    trainned_file = ''

    def __init__(self, trainned_file="classifier.pkl"):
        self.trainned_file = trainned_file
        if os.path.exists(trainned_file):
            print "loaded from file"
            self.model = joblib.load(trainned_file)
        else:
            self._build_untrainned_model()
            print "created new classfier"

    def _build_untrainned_model(self):
        '''pipeline = Pipeline([('tfidf', TfidfTransformer()),
                             ('chi2', SelectKBest(chi2, k='all')),
                             ('nb', SVC())])
        self.model = SklearnClassifier(pipeline)'''
        self.model = None

    def classify_tweets(self, raw_tweets):
        '''tweets_features = []
        for result in raw_tweets:
            tweets_features.append(FreqDist(self.getFeatureVector(result.text.encode('utf-8'))))'''
        if self.corpus_tweets is None:
            self.create_corpus_tweets()

        result = []
        for tweet in raw_tweets:
            result.append(self.model.classify(getFeatures(self.get_clean_text(tweet.text))))
        return result

    def create_corpus_tweets(self):
        global wordFeatures
        self.load_train_data()
        corpus_tweets = []
        for (words, sentiment) in self.tweets:
            wordsFiltered = [e.lower() for e in self.get_clean_text(words) if len(e) >= 3]
            corpus_tweets.append((wordsFiltered, sentiment))

        wordFeatures = self.wordFeatures(self.bagOfWords(corpus_tweets))
        self.corpus_tweets = corpus_tweets
        self.tweets = None

    def train(self):

        self._build_untrainned_model()

        self.create_corpus_tweets()


        training = nltk.classify.apply_features(getFeatures, self.corpus_tweets)
        self.model = nltk.NaiveBayesClassifier.train(training)
        #data_set = [(FreqDist(self.getFeatureVector(t)), l) for t, l in tweets]
        #self.model.train(data_set)
        print(self.model.show_most_informative_features(32))
        self.save_model()


    def save_model(self):
        joblib.dump(self.model, self.trainned_file, compress=True)


    def processTweet(self, tweet):
        # process the tweets
        # Convert to lower case
        tweet = tweet.lower()
        # Convert www.* or https?://* to URL
        tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', 'URL', tweet)
        # Convert @username to AT_USER
        tweet = re.sub('@[^\s]+', 'AT_USER', tweet)
        # Remove additional white spaces
        tweet = re.sub('[\s]+', ' ', tweet)
        # Replace #word with word
        tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
        # trim
        tweet = tweet.strip('\'"')
        return tweet


    # start replaceTwoOrMore
    def replaceTwoOrMore(self, s):
        # look for 2 or more repetitions of character and replace with the character itself
        pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
        return pattern.sub(r"\1\1", s)

    def get_clean_text(self, tweet):
        featureVector = []
        # split tweet into words
        words = self.processTweet(tweet).split()
        for w in words:
            # replace two or more with two occurrences
            w = self.replaceTwoOrMore(w)
            # strip punctuation
            w = w.strip('\'"?,.')
            # check if the word stats with an alphabet
            val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
            #ignore if it is a stop word
            if (w in self.stopWords or val is None):
                continue
            else:
                featureVector.append(w.lower())
        return featureVector


    def bagOfWords(self,tweets):
        wordsList = []
        for (words, sentiment) in tweets:
            wordsList.extend(words)
        return wordsList


    def wordFeatures(self, wordList):
        wordList = nltk.FreqDist(wordList)
        wordFeatures = wordList.keys()
        return wordFeatures

    def load_train_data(self):
        self.tweets = []
        with open('tweets_train_data.csv', 'r') as train_data:
            reader = csv.DictReader(train_data, delimiter='|')
            for row in reader:
                self.tweets.append(
                    (row['text'].decode('utf-8'), row['class'])
                )




