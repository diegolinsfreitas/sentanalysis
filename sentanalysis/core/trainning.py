__author__ = 'diego'

from anylizer import SentimentClassifier
import os.path as path
import tweeter as thelper
import csv


def train():

    classifier = SentimentClassifier()
    classifier.load_train_data()
    classifier.train()
    '''
    k_fold = KFold(n=len(data_set), n_folds=2)
    scores = []

    for train_indices, test_indices in k_fold:
        print train_indices
        train_set = data_set[train_indices]
        #train_y = np.asarray(labels[train_indices])

        test_set = data_set[test_indices]
        #test_y = np.asarray(labels[test_indices])

        classif.train(train_set)
        result = np.array(classif.classify_many(test_set))
        print result
        #scores.append(score)

    #score = sum(scores) / len(scores)
    '''


def create_tweets_dataset():
    dataset_filename = 'tweets_train_data.csv'
    if not path.exists(dataset_filename):
        with open('tweets_train_data.csv', 'wb') as train_data:
            train_data.write('class|text\n')

    api = thelper.build_api()
    last_tweet_id = ''
    for i in range(5):
        results = api.search(geocode='-3.060002,-59.991321,30km', count=6, since_id=last_tweet_id)
        if len(results) == 0:
            continue
        last_tweet_id = results[0].id_str
        with open(dataset_filename, 'a+b') as train_data:
            for result in results:
                text = result.text.encode('utf-8')
                input = raw_input(text + '\n1:p 0:n blank:n>>>')
                if len(input) == 0:
                    class_ = 'neu'
                elif int(input) == 1:
                    class_ = 'pos'
                elif int(input) == 0:
                    class_ = 'neg'
                train_data.write("%s|%s\n" % ( class_, text.replace('\n', ' ') ))



