__author__ = 'diego'

from tastypie.resources import ModelResource, Resource, fields
from django.db.models import Count
from django.db import connection

from models import Sentiment


class PieChartModel(object):
    lastid=-1
    positive=20
    negative=40
    neutral=4


class PieChartModelResource(Resource):
    # Just like a Django ``Form`` or ``Model``, we're defining all the
    # fields we're going to handle with the API here.
    '''uuid = fields.CharField(attribute='uuid')
    user_uuid = fields.CharField(attribute='user_uuid')
    message = fields.CharField(attribute='message')
    created = fields.IntegerField(attribute='created')'''

    lastid = fields.DateTimeField(attribute='lastid')
    positive = fields.IntegerField(attribute='positive')
    negative = fields.IntegerField(attribute='negative')
    neutral = fields.IntegerField(attribute='neutral')

    class Meta:
        resource_name = 'chartmodel'
        object_class = PieChartModel

    def obj_get_list(self, bundle, **kwargs):
        lastid = bundle.request.GET.get('lastid')
        if lastid is None:
            lastid = -1
        model = PieChartModel()
        model.lastid = Sentiment.objects.all().order_by('-id')[0].id
        cursor = connection.cursor()
        cursor.execute("select count(*) as count from sentanalysis.sentiment_sentiment where id > %s and feeling like 'pos'", [lastid])
        model.positive = cursor.fetchone()[0]
        cursor.execute("select count(*) as count from sentanalysis.sentiment_sentiment where id > %s and feeling like 'neg'", [lastid])
        model.negative = cursor.fetchone()[0]
        cursor.execute("select count(*) as count from sentanalysis.sentiment_sentiment where id > %s and feeling like 'neu'", [lastid])
        model.neutral = cursor.fetchone()[0]
        return [model]


class SentimentResource(ModelResource):
    class Meta:
        resource_name = 'mood'
        queryset = Sentiment.objects.all()