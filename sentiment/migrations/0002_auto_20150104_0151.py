# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sentiment', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sentiment',
            name='tweet_id',
        ),
        migrations.AlterField(
            model_name='sentiment',
            name='feeling',
            field=models.CharField(max_length=3),
            preserve_default=True,
        ),
    ]
