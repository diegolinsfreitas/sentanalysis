from django.core.management.base import BaseCommand, CommandError
from sentiment.models import Sentiment
from sentanalysis.core.tweeter import SentimentsLoader


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    #def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        SentimentsLoader().load_sentiments_from_twitter()