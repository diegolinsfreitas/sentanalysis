from django.db import models

class Sentiment(models.Model):
    pub_date = models.DateTimeField('date published')
    feeling = models.CharField(max_length=3)
