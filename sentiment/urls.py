__author__ = 'diego'

from django.conf.urls import patterns, url

from sentiment import views


from tastypie.api import Api
from resources import SentimentResource, PieChartModelResource

api = Api(api_name='api')
api.register(SentimentResource())
api.register(PieChartModelResource())


urlpatterns = patterns('',
    url(r'^$', views.index, name='index')
) +  api.urls
